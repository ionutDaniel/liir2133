package agenda.model.repository.classes;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class RepositoryContactFileTest {

    private RepositoryContactFile rep;
    private int repSize;


    @Before
    public void setUp() throws Exception {
        rep = new RepositoryContactFile();
        repSize = rep.count();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addContact() throws Exception {
        Contact c1 = new Contact("nume valid","andress","07123123");
        rep.addContact(c1);
        assertEquals(rep.count() ,repSize+1);
        repSize++;
    }

    // ECP - invalid phone number
    @Test(expected = InvalidFormatException.class)
    public void addContact2() throws Exception{
        Contact c1 = new Contact("nume valid2","das","234324");
        rep.addContact(c1);
        assertEquals(rep.count(),repSize);
    }

    // length = 3
    @Test
    public void addContactBVA1_valid() throws Exception{
        Contact c = new Contact("nnn","adresa","04234");
        rep.addContact(c);
        assertEquals(rep.count(),repSize+1);
    }


    // length = 47
    @Test
    public void addContactBVA3_valid() throws Exception{
        Contact c = new Contact("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn","adresa","04234");
        rep.addContact(c);
        assertEquals(rep.count(),repSize+1);
    }


    // length = 48
    @Test
    public void addContactBVA2_valid() throws Exception{
        Contact c = new Contact("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn","adresa","04234");
        rep.addContact(c);
        assertEquals(rep.count(),repSize+1);
    }

    // length = 4
//    @Test(expected = InvalidFormatException.class)
//    public void addContactBVA1() throws Exception{
//        Contact c = new Contact("nnnn","adresa","04234");
//        rep.addContact(c);
//        assertEquals(rep.count(),repSize);
//    }

    //lenght = 49
    @Test(expected = InvalidFormatException.class)
    public void addContactBVA2() throws Exception{
        Contact c5 = new Contact("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn","adresa","04234");
        rep.addContact(c5);
        assertEquals(rep.count(),repSize);
    }

}