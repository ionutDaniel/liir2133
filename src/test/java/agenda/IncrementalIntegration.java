package agenda;

import agenda.controller.ControllerTest;
import agenda.controller.F3ReportTest;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.classes.RepositoryContactFileTest;
import agenda.model.repository.classes.RepositoryUserFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.startApp.MainClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class IncrementalIntegration {

    RepositoryActivity activityRepo;
    RepositoryContact contactRepo;
    User user;
    ByteArrayOutputStream baos;
    RepositoryContactFileTest repositoryContactFileTest;
    ControllerTest controllerTest;
    F3ReportTest f3ReportTest;


    public void buildBufferedReader()
    {
        if (baos!=null)
        {
            try
            {
                baos.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        baos = new ByteArrayOutputStream();
    }


    public void writeToBaos(String toWrite)
    {
        try
        {
            baos.write((toWrite+"\n").getBytes());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    @Before
    public void setUp() throws Exception
    {
        contactRepo = new RepositoryContactFile();
        System.out.println("Setting up");
        activityRepo = new RepositoryActivityFile(contactRepo);
        RepositoryUserFile userRepo = new RepositoryUserFile();
        user = userRepo.getByUsername("q");
        repositoryContactFileTest = new RepositoryContactFileTest();
        controllerTest = new ControllerTest();
        f3ReportTest = new F3ReportTest();
    }


    @Test
    public void testF1(){
        try{
            repositoryContactFileTest.setUp();
        }
        catch (Exception e){
            assertTrue(false);
        }
        try {
            System.out.println("");
            repositoryContactFileTest.addContact();
            repositoryContactFileTest.addContact2();
            repositoryContactFileTest.addContactBVA1_valid();
            repositoryContactFileTest.addContactBVA3_valid();
            repositoryContactFileTest.addContactBVA2_valid();
            repositoryContactFileTest.addContactBVA2();
        }
        catch (Exception e){
            assertTrue(true);
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testF2(){
        try
        {
            controllerTest.setUp();
        }
        catch(Exception e)
        {
            Assert.assertTrue(false);
        }
        System.out.println("");
        controllerTest.testInstr();
        controllerTest.testInstr2();
        controllerTest.testLoops();
        controllerTest.testDecisions();
    }

    @Test
    public void testF3()
    {
        try
        {
            f3ReportTest.setUp();
        }
        catch(Exception e)
        {
            Assert.assertTrue(false);
        }

        f3ReportTest.testInstr();
        f3ReportTest.testInstr2();
    }


    @Test
    public void PA(){
        buildBufferedReader();
        writeToBaos("q");
        writeToBaos("q");
        writeToBaos("1");
        writeToBaos("numenume");
        writeToBaos("adresaOarecare");
        writeToBaos("+073232");
        writeToBaos("4");

        InputStream targetStream = new ByteArrayInputStream(baos.toByteArray());
        System.setIn(targetStream);

        PrintStream console = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.out.println("Redirecting System.out to BAOS");
        PrintStream baosPS = new PrintStream(baos);
        System.setOut(baosPS);
        MainClass.main(null);
        System.setOut(console);
        System.out.println("Redirecting System.out to CONSOLE");
        String result = new String(baos.toByteArray());
        if (result.contains("S-a adugat cu succes"))
        {
            assertTrue(true);
        }
        else
        {
            Assert.assertTrue(false);
        }

    }


    @Test
    public void PAB(){
        buildBufferedReader();
        writeToBaos("q");
        writeToBaos("q");
        writeToBaos("1");
        writeToBaos("numenume");
        writeToBaos("adresaOarecare");
        writeToBaos("+073232");
        writeToBaos("2");
        writeToBaos("descriereeee");
        writeToBaos("10/10/2010");
        writeToBaos("10:10");
        writeToBaos("10/10/2010");
        writeToBaos("10:011");
        writeToBaos("sdfs");
        writeToBaos("4");
        InputStream targetStream = new ByteArrayInputStream(baos.toByteArray());
        System.setIn(targetStream);

        PrintStream console = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.out.println("Redirecting System.out to BAOS");
        PrintStream baosPS = new PrintStream(baos);
        System.setOut(baosPS);
        MainClass.main(null);
        System.setOut(console);
        System.out.println("Redirecting System.out to CONSOLE");
        String result = new String(baos.toByteArray());


        List<Activity> a = MainClass.activityRep.getActivities();
        boolean found = false;
        for (Activity a2:a)
        {
            if (a2.getDescription().equals("descriereeee"))
            {
                found = true;
                if (a2.getContacts().size() == 1)
                {
                    Assert.assertTrue(true);
                }
                else
                {
                    Assert.assertTrue(false);
                }
            }
        }
        if (!found)
        {
            Assert.assertTrue(false);
        }
        else
        {
            Assert.assertTrue(true);
        }


    }




    @Test
    public void PABC(){

        buildBufferedReader();
        writeToBaos("q");
        writeToBaos("q");
        writeToBaos("1");
        writeToBaos("numenume");
        writeToBaos("adresaOarecare");
        writeToBaos("+073232");
        writeToBaos("2");
        writeToBaos("descriere");
        writeToBaos("10/10/2010");
        writeToBaos("10:10");
        writeToBaos("10/10/2010");
        writeToBaos("10:011");
        writeToBaos("sdfs");
        writeToBaos("3");
        writeToBaos("10/10/2010");
        writeToBaos("4");

        InputStream targetStream = new ByteArrayInputStream(baos.toByteArray());
        System.setIn(targetStream);

        PrintStream console = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.out.println("Redirecting System.out to BAOS");
        PrintStream baosPS = new PrintStream(baos);
        System.setOut(baosPS);
        MainClass.main(null);
        System.setOut(console);
        System.out.println("Redirecting System.out to CONSOLE");
        String result = new String(baos.toByteArray());
        if (result.contains("sdfs"))
        {
            Assert.assertTrue(true);
        }
        else
        {
            Assert.assertTrue(false);
        }
    }

}
