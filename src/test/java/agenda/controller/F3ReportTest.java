package agenda.controller;

import agenda.model.base.User;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.classes.RepositoryUserFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static junit.framework.TestCase.assertTrue;

public class F3ReportTest {


    RepositoryActivity activityRepo;
    RepositoryContact contactRepo;
    User user;
    ByteArrayOutputStream baos;


    public void buildBufferedReader()
    {
        if (baos!=null)
        {
            try
            {
                baos.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        baos = new ByteArrayOutputStream();
    }

    public void writeToBaos(String toWrite)
    {
        try
        {
            baos.write((toWrite+"\n").getBytes());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    public BufferedReader getReader()
    {
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        return new BufferedReader(new InputStreamReader(bais));
    }


    @Before
    public void setUp() throws Exception
    {
        contactRepo = new RepositoryContactFile();
        activityRepo = new RepositoryActivityFile(contactRepo);
        RepositoryUserFile userRepo = new RepositoryUserFile();
        user = userRepo.getByUsername("q");
    }


    @Test
    public void testInstr()
    {

        buildBufferedReader();
        writeToBaos("11/11/1111");

        PrintStream console = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.out.println("Redirecting System.out to BAOS");
        PrintStream baosPS = new PrintStream(baos);
        System.setOut(baosPS);
        Controller.afisActivitate(activityRepo,getReader(),user);
        System.setOut(console);
        System.out.println("Redirecting System.out to CONSOLE");
        String result = new String(baos.toByteArray());
        System.out.println(result);
        try
        {
            baos.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        if (result.contains("sport"))
        {
            assertTrue(true);
        }
        else
        {
            assertTrue(false);
        }
    }


    @Test
    public void testInstr2()
    {
        buildBufferedReader();
        writeToBaos("11/11/1111");

        PrintStream console = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.out.println("Redirecting System.out to BAOS");
        PrintStream baosPS = new PrintStream(baos);
        System.setOut(baosPS);
        Controller.afisActivitate(activityRepo,getReader(),user);
        System.setOut(console);
        System.out.println("Redirecting System.out to CONSOLE");
        String result = new String(baos.toByteArray());
        try
        {
            baos.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        if (result.contains("Sat Nov 11"))
        {
            Assert.assertTrue(true);
        }
        else
        {
            Assert.assertTrue(false);
        }
        if (result.contains("description1") && result.contains("description2"))
        {
            Assert.assertTrue(false);
        }
        else
        {
            Assert.assertTrue(true);
        }
    }

}
